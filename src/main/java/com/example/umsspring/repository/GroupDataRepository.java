package com.example.umsspring.repository;

import com.example.umsspring.entity.GroupDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupDataRepository extends JpaRepository<GroupDataEntity, Long> {
}
//aaa