package com.example.umsspring.repository;



import com.example.umsspring.entity.AdminEntity;
import org.springframework.data.jpa.repository.JpaRepository;

//AAA
public interface AdminRepository extends JpaRepository<AdminEntity, Long> {
}
