package com.example.umsspring.controller;

import com.example.umsspring.entity.StudentEntity;
import com.example.umsspring.service.StudentServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class StudentController {
    @Autowired
    StudentServices studentServices;
    //aaa
    @GetMapping("/students")
    public List<StudentEntity> getStudents(){
        return studentServices.getStudents();
    }

    @PostMapping("/admin/create-student")
    public void createStudent(){
        studentServices.createStudent();
    }
}
