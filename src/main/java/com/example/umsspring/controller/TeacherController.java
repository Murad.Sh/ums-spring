package com.example.umsspring.controller;

import com.example.umsspring.entity.TeacherEntity;
import com.example.umsspring.service.TeacherServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TeacherController {
    @Autowired
    TeacherServices teacherServices;

    //aaa
    @GetMapping("/teachers")
    public List<TeacherEntity> getTeachers() {
//        return "pox";
        return teacherServices.getTeachers();
    }

    @PostMapping("/admin/create-teacher")
    public void createTeacher() {
        teacherServices.createTeacher();
    }

}
