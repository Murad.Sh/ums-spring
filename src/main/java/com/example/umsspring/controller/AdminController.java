package com.example.umsspring.controller;

import com.example.umsspring.entity.AdminEntity;
import com.example.umsspring.service.AdminServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
//@RequestMapping("/admin")

public class AdminController {
    @Autowired
    AdminServices adminServices;

    @GetMapping("/test")
    public String test(){
        return "test";
    }

    @GetMapping("/admins")
    public List<AdminEntity> getAdmins(){
        //Terane hanim axirima chixacaq :D

        System.out.println("get adminsss");
        return adminServices.getAdmins();
    }

    @PostMapping("/create-admin")
    public void createAdmin(){
        adminServices.createAdmin();
    }

}
