package com.example.umsspring.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)

public class JournalData {

   Long id;
   Long groupId;
   Long journalId;
   Long teacherId;
   Long studentId;
   String date;
   String attendance;

}
//aaa