package com.example.umsspring.entity;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "group_data")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GroupDataEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    //aaa
    @ManyToOne
    @JoinColumn(name = "group_id")
    GroupEntity group;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    TeacherEntity teacher;
}
