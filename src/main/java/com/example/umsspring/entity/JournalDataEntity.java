package com.example.umsspring.entity;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "journal_data")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JournalDataEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    //aaa
    @ManyToOne
    @JoinColumn(name = "group_id")
    GroupEntity group;

    @ManyToOne
    @JoinColumn(name = "journal_id")
    JournalEntity journal;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    TeacherEntity teacher;

    @ManyToOne
    @JoinColumn(name = "student_id")
    StudentEntity student;

    @Column(name = "date")
    String date;

    @Column(name = "attendance")
    String attendance;
}
