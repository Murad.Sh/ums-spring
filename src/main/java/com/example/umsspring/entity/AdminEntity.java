package com.example.umsspring.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "admin")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    @Column(name = "name")
    String name;

    //AAA
    @Column(name = "surname")
    String surname;

    @Column(name = "username")
    String username;

    @Column(name = "password")
    String password;

    @Column(name = "role_id")
    Long roleId;

}
