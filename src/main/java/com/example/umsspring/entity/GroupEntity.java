package com.example.umsspring.entity;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "grup")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GroupEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    //aaa
    @Column(name = "group_name")
    String name;


}
