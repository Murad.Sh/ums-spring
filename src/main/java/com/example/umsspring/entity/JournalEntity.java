package com.example.umsspring.entity;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "journal")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JournalEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    //aaa
    @ManyToOne
    @JoinColumn(name = "group_id")
    GroupEntity groupEntity;

}
