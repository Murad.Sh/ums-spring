package com.example.umsspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmsSpringApplication {

	//AAA
	public static void main(String[] args) {
		SpringApplication.run(UmsSpringApplication.class, args);
	}

}
