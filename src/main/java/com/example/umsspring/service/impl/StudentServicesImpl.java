package com.example.umsspring.service.impl;


import com.example.umsspring.entity.GroupEntity;
import com.example.umsspring.entity.StudentEntity;
import com.example.umsspring.repository.GroupRepository;
import com.example.umsspring.repository.StudentRepository;
import com.example.umsspring.service.StudentServices;
import com.example.umsspring.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class StudentServicesImpl implements StudentServices {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    UserServices userServices;

    public List<StudentEntity> getStudents(){
        return studentRepository.findAll();
    }

    public void createStudent(){
        System.out.println("-------------------------");
        System.out.println("Sagird elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("name:");
        String name = scanner.next();
        System.out.println("surname:");
        String surname = scanner.next();
        System.out.println("username:");
        String username = scanner.next();
        System.out.println("password:");
        String password = scanner.next();
        System.out.println("group id:");
        Long groupId = scanner.nextLong();

        GroupEntity group = new GroupEntity();
        group.setId(groupId);
        Optional<GroupEntity> optionalGroup = groupRepository.findById(groupId);
        String groupName = optionalGroup.stream().map(item-> item.getName()).toString();
        group.setName(groupName);


        Long roleId = 3L;

        StudentEntity student = new StudentEntity(null,name, surname, username, password, group,roleId);

        studentRepository.save(student);
        userServices.createUser(name,surname,username,password,roleId);

    }
}
