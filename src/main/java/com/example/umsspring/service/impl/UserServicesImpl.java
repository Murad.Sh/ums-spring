package com.example.umsspring.service.impl;

import com.example.umsspring.entity.UserEntity;
import com.example.umsspring.repository.UserRepository;
import com.example.umsspring.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServicesImpl implements UserServices {
    @Autowired
    UserRepository userRepository;


    public void createUser(String name, String surname, String username, String password, Long roleId){
        UserEntity user = new UserEntity(null,name,surname,username,password,roleId);
        userRepository.save(user);
    }
}
