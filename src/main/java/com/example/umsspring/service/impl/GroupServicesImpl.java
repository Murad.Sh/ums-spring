package com.example.umsspring.service.impl;



import com.example.umsspring.entity.GroupEntity;
import com.example.umsspring.repository.GroupRepository;
import com.example.umsspring.service.GroupServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class GroupServicesImpl implements GroupServices {

    static Scanner scanner = new Scanner(System.in);

    @Autowired
    GroupRepository groupRepository;

    public void createGroup(){
        System.out.println("-------------------------");
        System.out.println("Qrup elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("Qrupun adi:");
        String groupName = scanner.next();

        GroupEntity group = new GroupEntity(null, groupName);
        groupRepository.save(group);
    }

    public List<GroupEntity> getGroups(){
        return  groupRepository.findAll();
    }


}
