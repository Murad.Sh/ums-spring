package com.example.umsspring.service;

import com.example.umsspring.entity.JournalDataEntity;

import java.util.List;

public interface JournalDataServices {
    void createJournalData();
    List<JournalDataEntity> getJournalDatas();
}
//aaa