package com.example.umsspring.service;

import com.example.umsspring.entity.GroupEntity;

import java.util.List;

public interface GroupServices {
    void createGroup();
    List<GroupEntity> getGroups();
}
//aaa