package com.example.umsspring.service;

import com.example.umsspring.entity.StudentEntity;

import java.util.List;

public interface StudentServices {

    void createStudent();
    List<StudentEntity> getStudents();
}
//aaa