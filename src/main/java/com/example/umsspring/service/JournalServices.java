package com.example.umsspring.service;

import com.example.umsspring.entity.JournalEntity;

import java.util.List;

public interface JournalServices {
    void createJournal();
    List<JournalEntity> getJournals();
}
//aaa