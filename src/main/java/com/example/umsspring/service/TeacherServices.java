package com.example.umsspring.service;

import com.example.umsspring.entity.TeacherEntity;

import java.util.List;

public interface TeacherServices {
    void createTeacher();
    List<TeacherEntity> getTeachers();
}
//aaa